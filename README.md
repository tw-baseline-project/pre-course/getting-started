Hi! Welcome to the pre-course section of the training. In this section, we will make sure you have the basic knowledge of frontend and backend programming so that we can continue to the next level.

So keep in mind that we aim to master the basic programming skill rather than to complete the homework.

## Typical Stories

### Encouraged

* John is participating in the pre-course program. He watches videos and practices in the interactive course. When he is doing exercise, he puts references away and records the time used. When the exercise is done, he will carefully examine his result against the answer to find improvement. And he will also check whether the time used is in the recommended range.

### Not encouraged

* John is participating in the pre-course program. To complete the exercise quickly, he has online references on the left hand and the exercise on the right hand. And he does nothing more once the exercise is ended. In the end, he still cannot complete a basic program in the examination.

## Pre-course content

Please check out the content of the pre-course and the corresponding online resources.

|Precourse content|Suggested online courses (no VPN required)|Suggested online courses (VPN required)|
|---|---|---|
|Java fundamental|[Learn Java I](https://www.imooc.com/learn/85) and [Learn Java II](https://www.imooc.com/learn/124) and [Learn Java III](https://www.imooc.com/learn/110)|[Learn Java](https://www.codecademy.com/learn/learn-java)|
|Javascript & ES6|[Javascript](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/)|[Introduction to Javascript](https://www.codecademy.com/learn/introduction-to-javascript)|
|HTML|[Basic HTML & HTML 5](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/)|[Learn HTML](https://www.codecademy.com/learn/learn-html)|
|CSS|[Basic CSS](https://www.freecodecamp.org/learn/responsive-web-design/basic-css/)|[Learn CSS](https://www.codecademy.com/learn/learn-css)|

The requirement for each content

![](source/images/Pre-course.png)

## Examination

We will use online courses with a step-by-step evaluating process to help you quickly get started. Each item in the content will have one or more online courses, self-evaluating exercises with answers and an examination. Though the examination result is one of the inputs of the final report, the main purpose is to help you to improve. Here is the schedule for the examination.

![](source/images/Pre-course-schedule.png)

You can see that we have 2 examinations for each item. You can choose either one to participate in. We will send you the requirement and you have to submit the answer in time.

## Tracking your progress

We have prepared a checklist for you to track your progress. If you are not sure what to do next, please go there for help.

https://trello.com/b/ysHUKakv/progress-tracking

When you have finished a certain task, please mark the item as DONE. If you are far behind schedule, we will remind you in the Wechat group (we do not want to do that though).

## Support

Asking questions is greatly encouraged and will be recorded. Please leave a message in the Wechat group if you have a question. We will reply as soon as we can (typically in an hour).